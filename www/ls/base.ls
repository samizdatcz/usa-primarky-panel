czechName = ig.czechName
container = d3.select ig.containers.base
panelContainer = container.append \div
  ..attr \class "panel generic"
  ..append \div
    ..attr \class \header
    ..append \h2
      ..html "Americké prezidentské volby 2016"
    ..append \h3
      ..html "Výsledky primárek v jednotlivých státech"
content = panelContainer.append \div
  ..attr \class \content

mapContainer = content.append \div
  ..attr \class \left-column

map = new ig.UsMap mapContainer

explainer = mapContainer.append \p
  ..attr \class \explainer

getCandidatesWithDelegates = (state, candidates) ->
  candidates
    .map (candidate) ->
      count = state.delegates[candidate.name]
      {candidate, count}
    .filter (.count)
    .sort (a, b) -> b.count - a.count
    .slice 0, 2

humanDate = ->
  return "#{it.getDate!}. #{it.getMonth! + 1}. #{it.getFullYear!}"

rightColumn = content.append \div
  ..attr \class \right-column
table = rightColumn.append \table
table.append \thead
  ..append \tr
    ..append \th .html "Stát"
    ..append \th .html "Demokraté"
    ..append \th .html "Republikáni"
tbody = table.append \tbody
now = Date.now!
top =
  "Florida"
  "Illinois"
  "Missouri"
  "North Carolina"
  "Ohio"
states_sorted = ig.data_processed.states.slice!
  ..sort (a, b) ->
    aIndex = top.indexOf a.name
    bIndex = top.indexOf b.name
    # console.log aIndex, bIndex
    if aIndex > -1 and bIndex == -1
      -1
    else if aIndex == -1 and bIndex > -1
      1
    else if aIndex > -1 and bIndex > -1
      aIndex - bIndex
    else
      aTime = (Math.max a.meta.democratsDate.getTime!, a.meta.republicansDate.getTime!)
      bTime = (Math.max b.meta.democratsDate.getTime!, b.meta.republicansDate.getTime!)
      aTime - bTime

tableRows = tbody.selectAll \tr .data states_sorted .enter!append \tr
tableRowName = tableRows.append \td .html -> czechName it.name
tableRowDate1 = tableRows.append \td
  ..selectAll \img .data (-> getCandidatesWithDelegates it, ig.data_processed.democratCandidates) .enter!append \img
    ..attr \class \candidate
    ..attr \alt (.candidate.name)
    ..attr \src -> "https://samizdat.cz/data/usa-primarky-panel/www/media/candidates/resized/#{it.candidate.id}.jpg"
  ..append \span
    ..attr \class \date
    ..html -> humanDate it.meta.democratsDate
tableRowDate2 = tableRows.append \td
  ..selectAll \img .data (-> getCandidatesWithDelegates it, ig.data_processed.republicanCandidates) .enter!append \img
    ..attr \class \candidate
    ..attr \alt (.candidate.name)
    ..attr \src -> "media/candidates/resized/#{it.candidate.id}.jpg"
  ..append \span
    ..attr \class \date
    ..html -> humanDate it.meta.republicansDate

fixedHeader = content.append \div
  ..attr \class \fixed-header
  ..append \div .html "Stát"
  ..append \div .html "Demokraté"
  ..append \div .html "Republikáni"

rightColumnNode = rightColumn.node!
highlightState = (stateName) ->
  tableRows.classed \active -> it.name is stateName
  map.states.classed \active -> it.properties.NAME is stateName
  row = tbody.node!querySelector \tr.active
  ig.scrollToElement rightColumnNode, row, offset: 167
  explainer.html "<b>Zvýrazněno:</b> #{czechName stateName}"

highlightStateFromTable = (stateName) ->
  map.states.classed \active -> it.properties.NAME is stateName
  tableRows.classed \active -> it.name is stateName
  explainer.html "<b>Zvýrazněno:</b> #{czechName stateName}"

highlightDate = (date) ->
  time = date.getTime!
  names = ig.data_processed.states
    .filter ->
      it.meta.democratsDate.getTime! is time or it.meta.republicansDate.getTime! is time
    .map (.name)
  map.states.classed \active -> it.properties.NAME in names
  tableRowDate1.classed \active -> it.meta.democratsDate.getTime! is time
  tableRowDate2.classed \active -> it.meta.republicansDate.getTime! is time
  explainer.html "<b>Zvýrazněno:</b> Státy, kde budou #{humanDate date} primárky (#{names.map czechName .join ', '})"

downlight = ->
  tbody.selectAll \.active .classed \active no
  names = getVotedStatesNames!
  map.states.classed \active -> it.properties.NAME in names
  explainer.html "<b>Zvýrazněno:</b> Státy, kde již proběhly primárky"

highlightCandidate = (candidate) ->
  names = ig.data_processed.states
    .filter -> it.delegates[candidate.name]
    .map (.name)
  map.states.classed \active -> it.properties.NAME in names
  tableRows.classed \active -> it.meta.name in names
  genderSuffix = if candidate.name == 'Hillary Clinton' then 'a' else ''
  explainer.html "<b>Zvýrazněno:</b> Státy, kde získal#genderSuffix delegáty #{candidate.name} (#{names.map czechName .join ', '})"

getVotedStatesNames = ->
  now = Date.now!
  # now = new Date!
  #   ..setMonth 1
  # now .= getTime!
  ig.data_processed.states
    .filter ->
      it.meta.democratsDate.getTime! < now or it.meta.republicansDate.getTime! < now
    .map (.name)

map.on \mouseover -> highlightState it.properties.NAME
map.on \mouseout -> downlight!
tableRowName.on \mouseover ({name}) -> highlightStateFromTable name
tableRowName.on \touchstart ({name}) -> highlightStateFromTable name
tableRowName.on \mouseout -> downlight!
tableRowDate1.select \.date
  ..on \mouseover -> highlightDate it.meta.democratsDate
  ..on \touchstart -> highlightDate it.meta.democratsDate
tableRowDate2.select \.date
  ..on \mouseover -> highlightDate it.meta.republicansDate
  ..on \touchstart -> highlightDate it.meta.republicansDate
tableRows.selectAll \img
  ..on \mouseover -> highlightCandidate it.candidate
  ..on \touchstart -> highlightCandidate it.candidate
tableRowDate1.on \mouseout -> downlight!
tableRowDate2.on \mouseout -> downlight!

downlight!
