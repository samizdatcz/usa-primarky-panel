czechName = ig.czechName
class Panel
  (@name, @party) ->

panels =
  new Panel "Demokratická strana" "democrats"
  new Panel "Republikánská strana" "republicans"

candidates = ig.data_processed.candidates

container = d3.select ig.containers.base

(panel) <~ panels.forEach
panelContainer = container.append \div
  ..attr \class "panel party #{panel.party}"
  ..append \div
    ..attr \class \header
    ..append \h2
      ..html panel.name
    ..append \h3
      ..html "Pořadí kandidátů"


content = panelContainer.append \div
  ..attr \class \content

mapContainer = content.append \div
  ..attr \class \left-column

map = new ig.UsMap mapContainer

partyCandidates = candidates.filter (-> it.party is panel.party)

rightColumn = content.append \div
  ..attr \class "right-column"
  ..append \div
    ..attr \class \fixed-header
    ..append \div .html "Delegátů"
    ..append \div .html "Získáno hlasů"
candidatesContainer = rightColumn.append \ul

candidateElements = candidatesContainer.selectAll \li .data partyCandidates .enter!append \li
  ..append \div
    ..attr \class \img
    ..append \img
      ..attr \src -> "https://samizdat.cz/data/usa-primarky-panel/www/media/candidates/resized/#{it.id}.jpg"
      ..attr \alt (.name)
  ..append \h4
    ..html (.name)
  ..append \div
    ..attr \class \delegates
  ..append \div
    ..attr \class \votes

displayCounts = (stateName) ->
  partyCandidates
    ..forEach (d, i) ->
      d.currentDelegates = if stateName then d.delegates[stateName] else d.delegateCount
      d.currentVotes = (if stateName then d.votes[stateName] else d.voteCount) || 0
    ..sort (a, b) ->
      (b.currentDelegates - a.currentDelegates || b.currentVotes - a.currentVotes)
    ..forEach (d, i) ->
      d.index = i
  votesTotal = partyCandidates.reduce do
    (prev, curr) -> prev + curr.currentVotes
    0
  resultsIn = no
  candidateElements
    ..classed \hidden -> it.index > if panel.party == \democrats then 1 else 2
    ..style \left -> "#{it.index * 150}px"
    ..select \.delegates
      ..html ->
        if not isNaN it.currentDelegates
          resultsIn := yes
          it.currentDelegates
        else
          "–"
    ..select \.votes
      ..html ->
        if votesTotal and not isNaN it.currentVotes
          "#{ig.utils.formatNumber it.currentVotes / votesTotal * 100, 1} %"
        else
          "–"
  return resultsIn


explainer = mapContainer.append \p
  ..attr \class \explainer

highlightCandidate = (candidate) ->
  names = ig.data_processed.states
    .filter -> it.delegates[candidate.name]
    .map (.name)
  map.states.classed \active -> it.properties.NAME in names
  genderSuffix = if candidate.name == 'Hillary Clinton' then 'a' else ''
  explainer.html "<b>Zvýrazněno:</b> Státy, kde získal#genderSuffix delegáty #{candidate.name} (#{names.map czechName .join ', '})"

getVotedStatesNames = ->
  now = Date.now!
  ig.data_processed.states
    .filter ->
      displayCounts it.name
    .map (.name)

downlight = ->
  names = getVotedStatesNames!
  map.states.classed \active -> it.properties.NAME in names
  party = if panel.party == "democrats" then "demokratické" else "republikánské"
  explainer.html "<b>Zvýrazněno:</b> Státy, kde již proběhly #party primárky"
  displayCounts null

highlightState = (stateName) ->
  map.states.classed \active -> it.properties.NAME is stateName
  resultsAreIn = displayCounts stateName
  if resultsAreIn
    explainer.html "<b>Zvýrazněno:</b> #{czechName stateName}"
  else
    explainer.html "<b>Zvýrazněno:</b> #{czechName stateName} – ve státě se zatím nevolilo"

candidateElements
  ..on \mouseover -> highlightCandidate it
  ..on \touchstart -> highlightCandidate it
  ..on \mouseout downlight
map
  ..on \mouseover -> highlightState it.properties.NAME
  ..on \mouseout downlight
downlight!
