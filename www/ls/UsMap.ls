topo = ig.data.usMap

features = topojson.feature topo, topo.objects.data .features

width = 300
height = 194

mesh = topojson.mesh topo, topo.objects.data
projection = d3.geo.albersUsa!
  ..translate [width/2, height/2]
  ..scale (400 / 290) * width
path = d3.geo.path!
  ..projection projection
bordersG = path mesh

class ig.UsMap
  (@parentElement) ->
    ig.Events @
    @svg = @parentElement.append \svg
      ..attr \class \us-map
      ..attr {width, height}
    @statesG = @svg.append \g
      ..attr \class \states
    @states = @statesG.selectAll \path .data features .enter!append \path
      ..attr \class \state
      ..attr \d path
      ..on \mouseover ~> @emit \mouseover it
      ..on \touchstart ~> @emit \mouseover it
      ..on \mouseout ~> @emit \mouseout it
    @borders = @svg.append \path
      ..attr \class \border
      ..attr \d bordersG
