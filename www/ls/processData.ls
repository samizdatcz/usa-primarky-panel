czechNames =
  "Alaska"         : "Aljaška"
  "California"     : "Kalifornie"
  "Hawaii"         : "Havaj"
  "North Carolina" : "Severní Karolina"
  "North Dakota"   : "Severní Dakota"
  "Virginia"       : "Virginie"
  "West Virginia"  : "Západní Virginie"

ig.czechName = -> czechNames[it] || it

class State
  (@name, @meta) ->


parseDate = ->
  [y, m, d] = it.split "-" .map parseInt _, 10
  date = new Date!
    ..setTime 0
    ..setHours 12
    ..setFullYear y
    ..setMonth m - 1
    ..setDate d
  date

states_meta = d3.tsv.parse ig.data.states_meta, (row) ->
  row.republicansDate = parseDate row['republicans-date']
  row.democratsDate = parseDate row['democrats-date']
  row.democratsDelegates = parseInt row['democrats-delegates'], 10
  row.republicansDelegates = parseInt row['republicans-delegates'], 10
  row.democratsSuperDelegates = parseInt row['democrats-superdelegates'], 10
  row.republicansSuperDelegates = parseInt row['republicans-superdelegates'], 10
  row
candidates = d3.tsv.parse ig.data.candidates, (row) ->
  row.isDemocrat = row.party == \democrats
  row
candidate_delegates = d3.tsv.parse ig.data.candidate_delegates, (row) ->
  for key, value of row
    continue if key == \state
    row[key] = parseInt value, 10
  row

candidate_votes = d3.tsv.parse ig.data.candidate_votes, (row) ->
  for key, value of row
    continue if key == \state
    row[key] = parseInt value, 10
  row

statesAssoc = {}
states = for stateMeta in states_meta
  statesAssoc[stateMeta.name] = new State stateMeta.name, stateMeta

for result in candidate_delegates
  statesAssoc[result.state].delegates = result
for result in candidate_votes
  statesAssoc[result.state].votes = result

for candidate in candidates
  candidate
    ..voteCount = 0
    ..delegateCount = 0
    ..votes = {}
    ..delegates = {}
  for state in states
    candidate
      ..delegateCount += state.delegates[candidate.name] || 0
      ..voteCount += state.votes[candidate.name] || 0
      ..delegates[state.name] = state.delegates[candidate.name]
      ..votes[state.name] = state.votes[candidate.name]

ig.data_processed = {states, states_meta, candidates, candidate_delegates}

ig.data_processed.republicanCandidates = ig.data_processed.candidates.filter (.isDemocrat is no)
ig.data_processed.democratCandidates = ig.data_processed.candidates.filter (.isDemocrat)

