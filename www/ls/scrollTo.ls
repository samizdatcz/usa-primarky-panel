ig.scrollTo = (container, offset) ->
  d3.transition!
    .duration 200
    .tween "scroll" scrollTween container, offset

ig.scrollToElement = (container, element, options = {}) ->
  options.offset ?= 0
  ig.scrollTo container, element.offsetTop - options.offset

scrollTween = (container, offset) ->
  container != window
  ->
    interpolate = d3.interpolateNumber do
      container.scrollTop
      offset
    (progress) -> container.scrollTop = interpolate progress
